let http = require("http");

// create a var "port" to store the port number
const port = 4000;

// create a var "server" that store the output of the "createServer" method
const server = http.createServer((request, response)=> {
	if (request.url == '/greeting'){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('hello again')
	}else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('this is the homepage')
	}else{
		response.writeHead(404,{'Content-Type': 'text/plain'})
		response.end('Page Not available')
	}

} )

// uses the "server" and "port" variable created above
server.listen(port)

// when server is running console will print the msg
console.log(`server now accessible at localhost: ${port}.`)