// NODE JS INTRODUCTION

// use the require directive to load node.js modules
// A module is a software component or part of a program that contains 1 or more routines
// the  "http module" lets NODE.JS transfer data using the HYPER TEXT TRANSFER PROTOCOL
// the "HTTP module" is a set of individula files that contain code to create a "component" that helps estrablish data transfer between application
// http is a protocol that allow the fetching of resources such a HTML documents.
// clients(browser) and servers(node.js/express.js application) communicate by exchanging individual msgs
// the msg sent by the client, ussually , a web broser, are called requests
// the msg sent by the server as an answer are called responses

let http = require("http");

// the http module has a createServer method that accepts a function as an argument and allows for a creation of a server
// the argument passed in th function are request and response object that contains methods that allows iut to receive requests from the client and send responses bact to it.
http.createServer(function ( request, response) {
	/*
		Use the writeHead() method to :
		- set a status code for the respose- a 200 means "ok"
		- set the content-type of the response as a plain text message

	*/

	response.writeHead(200, {'Content-Type': 'text/plain'})
	// send the response w/ the text content "Hello world"
	response.end('hello world');

} ).listen(4000)

// a port is a virtual point where network connections start and end
// each port is associated w. a specific process or service
// the server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any request that are sent to is eventually communication w/ our server
console.log('server running at localhost: 4000')